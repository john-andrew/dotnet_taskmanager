﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TaskManager.Models
{
	public class Task
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public bool complete { get; set; }
	}

	public class TaskDBContext : DbContext
	{
		public DbSet<Task> Tasks { get; set; }
	}
}